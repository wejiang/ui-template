import * as actionTypes from "./actionTypes";

const ACTION_HANDLERS = {
	[actionTypes.TEST_REQUEST]: (state) => {
		return {
			...state,
			testLoading: true,
			testResult: null,
			testError: null,
		};
	},
	[actionTypes.TEST_SUCCESS]: (state, { testResult }) => {
		return {
			...state,
			testLoading: false,
			testResult,
		};
	},
	[actionTypes.TEST_FAIL]: (state, { testError }) => {
		return {
			...state,
			testLoading: false,
			testError,
		};
	},
};

const initialState = {
	testLoading: true,
	testResult: null,
	testError: null,
};

const mainReducer = (state = initialState, action) => {
	const handler = ACTION_HANDLERS[action.type];
	return handler ? handler(state, action) : state;
};

export default mainReducer;
