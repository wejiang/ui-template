import * as actionTypes from "./actionTypes";

export const testRequest = () => ({
	type: actionTypes.TEST_REQUEST,
});

export const testSuccess = (testResult) => ({
	type: actionTypes.TEST_SUCCESS,
	testResult,
});

export const testFail = (testError) => ({
	type: actionTypes.TEST_REQUEST,
	testError,
});
