import { ajax } from "rxjs/ajax";

export const getTestData = () => {
	return ajax({
		withCredentials: false,
		method: "GET",
		responseType: "json",
		url: "http://dummy.restapiexample.com/api/v1/employees",
	});
};
