import React from "react";
import { connect } from "react-redux";
import mainStoreInfo from "./store";

class Main extends React.Component {
	componentDidMount() {
		this.props.testRequest();
	}

	render() {
		const { testLoading, testError, testResult } = this.props;
		return testError ? (
			<div>Error Loading Test Data</div>
		) : testLoading ? (
			<div>Loading Test Data</div>
		) : (
			<div>{testResult}</div>
		);
	}
}

const mapStateToProps = (state) => ({ ...state.main });

const mapDispatchToProps = { ...mainStoreInfo.actions };

export default connect(mapStateToProps, mapDispatchToProps)(Main);
