import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import { createEpicMiddleware, combineEpics } from "redux-observable";
import mainStoreInfo from "./app/store";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const epicMiddleware = createEpicMiddleware();

const rootReducer = combineReducers({
	main: mainStoreInfo.reducer,
});

const rootEpic = combineEpics(...mainStoreInfo.epics);

const store = createStore(
	rootReducer,
	composeEnhancers(applyMiddleware(epicMiddleware))
);

export default store;

epicMiddleware.run(rootEpic);
