import mainReducer from "./reducer";
import mainEpics from "./epics";
import * as actions from "./actions";

export default {
	reducer: mainReducer,
	epics: mainEpics,
	actions: actions,
};
