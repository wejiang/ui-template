import { ofType } from "redux-observable";
import { catchError, mergeMap } from "rxjs/operators";
import { of } from "rxjs";

import * as actions from "./actions";
import * as actionTypes from "./actionTypes";
import * as services from "./services";

const fetchTestDataEpic = (action$, store) => {
	return action$.pipe(
		ofType(actionTypes.TEST_REQUEST),
		mergeMap(() => {
			return services.getTestData().pipe(
				mergeMap((response) => {
					return of(actions.testSuccess(response.response.message));
				}),
				catchError((error) => {
					return of(actions.testFail(error));
				})
			);
		})
	);
};

const mainEpics = [fetchTestDataEpic];

export default mainEpics;
